$(document).ready(function(){

    $('a.story-spawn').click(function(e) {
           var storyId = $(this).data('story');
           $.ajax({
               url: `stories/story-${storyId}.xml`,
               dataType: "xml"
           }).done(
               function( xmlDoc ) {
                   var x2js = new X2JS({
                   arrayAccessFormPaths : [
                       "story.page"
                   ]
                   });
                   var jsonObj = x2js.xml2json(xmlDoc);

                   var items = jsonObj.story.page.map(function(p) {
                       var size = p.img.split('_')[2].split('.')[0];
                       var width = size.split('x')[0];
                       var height = size.split('x')[1];
                        return {
                                   src: p.img,
                                   w: width,
                                   h: height,
                                   title: p.text
                               };
                   });

                   var pswpElement = document.querySelectorAll('.pswp')[0];

                   // define options (if needed)
                   var options = {
                       // optionName: 'option value'
                       // for example:
                       index: 0 // start at first slide
                   };

                   // Initializes and opens PhotoSwipe
                   var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                   gallery.init();

                   //console.log(items);
               }
           );


           // Cancel the default action
           e.preventDefault();
       });
});