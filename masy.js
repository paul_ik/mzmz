$(document).ready(function(){

    var storyId = window.location.href.split('story=')[1];

    $.ajax({
        url: `stories/story-${storyId}.xml`,
        dataType: "xml"
    }).done(
        function( xmlDoc ) {
            var x2js = new X2JS({
            arrayAccessFormPaths : [
                "story.page"
            ]
            });
            var jsonObj = x2js.xml2json(xmlDoc);
            var template = $.templates("#theTmpl");

            var htmlOutput = template.render(jsonObj);
            $("body").html(htmlOutput);

            $( '.mypage' ).on( "swipeleft", swipeHandler );
            $( '.mypage' ).on( "swiperight", swipeHandlerReversed );

            $(":mobile-pagecontainer").pagecontainer("change", $("#page1"), {'transition': 'flip'});
        }
    );

    function swipeHandler( event ){
        var loc = $.mobile.activePage.data("next");
        if(typeof loc !== "undefined") {
            $(":mobile-pagecontainer").pagecontainer("change", $(loc), {'transition': 'flip'});
        }
    }

    function swipeHandlerReversed( event ){
        var loc = $.mobile.activePage.data("prev");
        if(typeof loc !== "undefined") {
            $(":mobile-pagecontainer").pagecontainer("change", $(loc), {'transition': 'flip', 'reverse': true});
        }
  }
});
